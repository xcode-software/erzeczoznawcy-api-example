package pl.xcodesoftware.erzeczoznawcy;

import pl.xcodesoftware.erzeczoznawcy.client.ApiClientBuilder;
import pl.xcodesoftware.erzeczoznawcy.msg.IssueMessage;
import pl.xcodesoftware.erzeczoznawcy.msg.NoteMessage;
import pl.xcodesoftware.erzeczoznawcy.rest.IssuesEndpoint;
import pl.xcodesoftware.erzeczoznawcy.rest.NotesEndpoint;

import java.util.List;
import java.util.logging.Logger;

public class CreateNotesExampleApp {
    private static final Logger LOGGER = Logger.getLogger(CreateNotesExampleApp.class.getName());

    public static void main(String[] args) {
        CreateNotesExampleApp app = new CreateNotesExampleApp();
        IssueMessage newIssue = app.createDummyIssue();
        app.createNotes(newIssue.getId());
        app.fetchNotes(newIssue.getId());
    }

    private IssuesEndpoint issueClient;
    private NotesEndpoint noteClient;

    public CreateNotesExampleApp() {
        issueClient = ApiClientBuilder.newBuilder().buildIssuesClient();
        noteClient = ApiClientBuilder.newBuilder().buildNotesClient();
    }

    private IssueMessage createDummyIssue() {
        IssueMessage newIssue = new IssueMessage();

        IssueMessage createdIssue = issueClient.create(newIssue);

        LOGGER.info(String.format("new issue created with id: %s", createdIssue.getId()));

        return createdIssue;
    }

    private void createNotes(String issueId) {
        NoteMessage firstNote = new NoteMessage();
        firstNote.setIssueId(issueId);
        firstNote.setText("first message");

        NoteMessage secondNote = new NoteMessage();
        secondNote.setIssueId(issueId);
        secondNote.setText("second message");

        noteClient.create(firstNote);
        noteClient.create(secondNote);
    }

    private void fetchNotes(String issueId) {
        List<NoteMessage> notes = noteClient.fetch(issueId);

        LOGGER.info(String.format("notes: %s", notes.size()));
        LOGGER.info(notes.toString());
    }
}
