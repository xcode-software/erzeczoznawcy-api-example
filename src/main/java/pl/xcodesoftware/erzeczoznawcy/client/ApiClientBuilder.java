package pl.xcodesoftware.erzeczoznawcy.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.proxy.WebResourceFactory;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import pl.xcodesoftware.erzeczoznawcy.rest.GroupsEndpoint;
import pl.xcodesoftware.erzeczoznawcy.rest.IssuesEndpoint;
import pl.xcodesoftware.erzeczoznawcy.rest.NotesEndpoint;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class ApiClientBuilder {
    private static final int CONNECT_TIMEOUT = 1000;
    private static final int READ_TIMEOUT = 5000;
    private static final String API_URL = "http://autonet-test.erzeczoznawcy.eu";
    private static final String USER = "user-name";
    private static final String PASSWORD = "user-password";

    private WebTarget webTarget;

    private ApiClientBuilder() {
        prepareWebTarget();
    }

    public static final ApiClientBuilder newBuilder() {
        return new ApiClientBuilder();
    }

    public IssuesEndpoint buildIssuesClient() {
        return WebResourceFactory.newResource(IssuesEndpoint.class, webTarget);
    }

    public NotesEndpoint buildNotesClient() {
        return WebResourceFactory.newResource(NotesEndpoint.class, webTarget);
    }

    public GroupsEndpoint buildGroupsClient() {
        return WebResourceFactory.newResource(GroupsEndpoint.class, webTarget);
    }

    private void prepareWebTarget() {
        JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
        provider.setMapper(new ObjectMapper());

        Client client = ClientBuilder.newBuilder()
                .register(MultiPartFeature.class)
                .register(provider)
                .register(new ApiAuthenticator(USER, PASSWORD))
                .property(ClientProperties.CONNECT_TIMEOUT, CONNECT_TIMEOUT)
                .property(ClientProperties.READ_TIMEOUT, READ_TIMEOUT)
                .build();

        webTarget = client.target(API_URL);
    }
}
