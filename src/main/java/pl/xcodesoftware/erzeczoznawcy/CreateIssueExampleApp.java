package pl.xcodesoftware.erzeczoznawcy;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import pl.xcodesoftware.erzeczoznawcy.client.ApiClientBuilder;
import pl.xcodesoftware.erzeczoznawcy.msg.AttachmentMessage;
import pl.xcodesoftware.erzeczoznawcy.msg.IssueMessage;
import pl.xcodesoftware.erzeczoznawcy.rest.IssuesEndpoint;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class CreateIssueExampleApp {
    private static final Logger LOGGER = Logger.getLogger(CreateIssueExampleApp.class.getName());

    public static void main(String[] args) throws IOException {
        CreateIssueExampleApp app = new CreateIssueExampleApp();
        IssueMessage newIssue = app.createNewIssue();
        app.displayIssueData(newIssue.getId());
        app.uploadFile(newIssue.getId());
        app.getFileList(newIssue.getId());
    }

    private IssuesEndpoint issueClient;

    public CreateIssueExampleApp() {
        issueClient = ApiClientBuilder.newBuilder().buildIssuesClient();
    }

    private IssueMessage createNewIssue() {
        IssueMessage newIssue = new IssueMessage();
        newIssue.setClientIssueId("my issue id");
        newIssue.setCarModel("Toyota Corolla");
        newIssue.setCarLocationAddress("Warszawa");
        newIssue.setCarOwnerName("Tomasz Nowak");
        newIssue.setCarOwnerPhoneNumber("+48601602603");
        newIssue.setAdditionalInfo("additional info");

        IssueMessage createdIssue = issueClient.create(newIssue);

        LOGGER.info(String.format("new issue created with data:\n" +
                "id: %s\n" +
                "issueId: %s\n" +
                "clientIssueId: %s", createdIssue.getId(), createdIssue.getIssueId(), createdIssue.getClientIssueId()));

        return createdIssue;
    }

    private void displayIssueData(String issueId) {
        IssueMessage issueMessage = issueClient.get(issueId);

        LOGGER.info(issueMessage.toString());
    }

    private void uploadFile(String issueId) throws IOException {
        FormDataMultiPart formData = new FormDataMultiPart();

        File file = new File(CreateIssueExampleApp.class.getClassLoader().getResource("test.jpeg").getPath());
        formData.bodyPart(new FileDataBodyPart("attachment", file));

        issueClient.uploadAttachment(issueId, formData);
    }

    private void getFileList(String issueId) {
        List<AttachmentMessage> attachments = issueClient.getAttachments(issueId);

        LOGGER.info(attachments.toString());
    }
}
