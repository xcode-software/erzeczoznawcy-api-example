package pl.xcodesoftware.erzeczoznawcy;

import pl.xcodesoftware.erzeczoznawcy.client.ApiClientBuilder;
import pl.xcodesoftware.erzeczoznawcy.msg.GroupMessage;
import pl.xcodesoftware.erzeczoznawcy.rest.GroupsEndpoint;

import java.util.List;
import java.util.logging.Logger;

public class GroupsExampleApp {
    private static final Logger LOGGER = Logger.getLogger(GroupsExampleApp.class.getName());

    public static void main(String[] args) {
        GroupsExampleApp app = new GroupsExampleApp();
        app.fetchGroups();
    }

    private GroupsEndpoint groupsEndpoint;

    public GroupsExampleApp() {
        groupsEndpoint = ApiClientBuilder.newBuilder().buildGroupsClient();
    }

    private void fetchGroups() {
        List<GroupMessage> groups = groupsEndpoint.fetch();

        LOGGER.info(String.format("groups: %s", groups.size()));
        LOGGER.info(groups.toString());
    }
}
